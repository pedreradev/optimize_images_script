<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Optimize Images</title>
</head>
<body>
	<?php 
		error_reporting(E_ALL);
		ini_set('display_errors', 1);

		include('vendor/optimize_image_directory.php');

		$folder = "../uploads/general/";
		$images = optimize_drawImages($folder, 30);
	?>
</body>
</html>