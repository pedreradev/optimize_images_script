# Optimize Images Script #

This tool allows you to get images from a specific folder and import them into [Google PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/)

Just upload this folder, specify a directory, and add the link to Google PageSpeed Insights to download your optimized images!



This tool was made by [Pedrera Inc](http://www.pedrera.com).