<?php

/*
 * Gets images from directory.
 * More solutions and discussion here: http://stackoverflow.com/questions/17122218/get-all-the-images-from-a-folder-in-php
 */
function optimize_getImagesfromFolder($folder){
    $images = [];
    $imageTypes = [ 'jpg', 'jpeg', 'png', 'gif' ];

    if (file_exists($folder) == false) {
    	echo "Directory not found.";
        return ["Directory \'', $folder, '\' not found!"];
    } 
    else {
        $directory = scandir($folder);
        foreach ($directory as $file) {
            $file_type = pathinfo($file, PATHINFO_EXTENSION);
            if (in_array($file_type, $imageTypes) == true) {
                $images[] = $file;
            }
        }
        return $images;
    }
}

/*
 * Draws images from a folder
 *
 * $folder: Which directory to look into
 * $page: How many files per page
 *
 */
function optimize_drawImages($folder, $perPage=50) {
	ob_start();

	$images = optimize_getImagesfromFolder($folder); 
	$current_page = sanitizePageNumber();
?>
	
	<div class="col-md-12">
		<h3>Images from: <?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".$folder; ?></h3>
		<h4>Page <?php echo $current_page; ?></h4>
	</div>

	<?php echo drawPagination($current_page); ?>
	
	<table border="0">
		<?php 
		for($i=(($perPage*($current_page))-$perPage);$i<($perPage*$current_page);$i++) {
			echo '<tr><td><img src="'.$folder ."/". $images[$i] .'" border="0" /></td></tr>';
		} ?>
	</table>
	
	<?php
	$output = ob_get_contents();
	ob_end_clean();
	echo $output;
}

/* Sanitize Page input */
function sanitizePageNumber() {
	if (isset($_GET['page'])) {
		return preg_replace("/[^0-9,.]/", "", $_GET['page']);
	} else {
		return 1;
	}
}

function drawPagination($current_page) {
	ob_start(); ?>
	
	<section class="pagination">
		<?php if ($current_page-1 > 0) { ?>
			<a href="?page=<?php echo $current_page-1; ?>">previous</a> &nbsp;&nbsp;|&nbsp;&nbsp;
		<?php } else { ?>
			<a disabled="disabled">previous</a> &nbsp;&nbsp;|&nbsp;&nbsp;
		<?php } ?>
		<a href="?page=<?php echo $current_page+1; ?>">next</a>
	</section>

	<?php
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
}